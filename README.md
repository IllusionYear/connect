# Connect

现在社会里人与人之间的关系越来越冷淡，所以这里看到的每个表情都是没有色彩的。但是当你主动伸出手与他人互动，就会发现彼此变成了彩色，经过交流与碰撞，身后也留下了一串彩色的印记。这个互动投影表达的主题就是，交流让世界更美好。