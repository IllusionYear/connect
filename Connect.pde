int pn = 100;
int maxSpeed = 4;
int delta = 200;
PVector[] location = new PVector[pn];
PVector[] veclocity = new PVector[pn];
ArrayList tri;
int times=1;
PImage[] images_color;
PImage[] images_gray;
void setup() {
  fullScreen();
  background(0);
  smooth();
  for (int i=0; i<pn; i++) {
    location[i] = new PVector(random(0, width), random(0, height));
    veclocity[i] = new PVector(random(-maxSpeed, maxSpeed), random(-maxSpeed, maxSpeed));
  }
  tri = new ArrayList();
  frameRate(20);

  //load images
  images_color = new PImage[pn];
  images_gray = new PImage[pn];
  int index;
  //println(dataPath(""));
  for (int i = 0; i < images_color.length; i++) {
    index = (int)random(1, 45);
    images_color[i] = loadImage("img_ (" + index + ").png");
    images_gray[i] = loadImage("img_" + index + ".png");
  }
  imageMode(CENTER);
}

void draw() {
  background(0);
  noStroke();
  fill(255, 255);
  float dst=0;
  strokeWeight(8); 
  for (int i=0; i<pn; i++) {
    for (int j=i; j<pn; j++) {
      dst = dist( location[i].x, location[i].y, location[j].x, location[j].y );
      if ( dst < delta ) { 
        //stroke( 255, 255 - (dst*2>255?255:dst*2) ); 
        stroke( 255, 255 - dst/delta*255 ); 
        line( location[i].x, location[i].y, location[j].x, location[j].y );
      }
    }
  }
  for (int i=0; i<pn; i++) {
    location[i].add(veclocity[i]);
    if ((location[i].x > width) || (location[i].x < 0)) {
      veclocity[i].x = veclocity[i].x*-1;
    }
    if ((location[i].y > height) || (location[i].y < 0)) {
      veclocity[i].y = veclocity[i].y*-1;
    }
    //ellipse(location[i].x, location[i].y, 5, 5);
    image(images_gray[i], location[i].x, location[i].y, 50, 50);
    //filter(GRAY);
  }
  for (int i=0; i<tri.size(); i++) {
    Triangle t = (Triangle)tri.get(i);
    t.update();
    if (t.l<0)
      tri.remove(i);
  }
}
void mouseMoved() {
  float dst=0;
  point(mouseX, mouseY);
  for (int j=0; j<pn; j++) {
    dst = dist( mouseX, mouseY, location[j].x, location[j].y );
    if ( dst < delta ) { 
      stroke( 255, 165, 0, delta - dst ); 
      line( mouseX, mouseY, location[j].x, location[j].y );
      image(images_color[j], location[j].x, location[j].y, 50, 50);
    }
  }
  times++;
  if (times%4==0) {
    PVector m=new PVector(mouseX, mouseY);
    tri.add(new Triangle(m));
  }
}
class Triangle {
  float x1, y1, x2, y2, x3, y3;
  int cr, cg, cb;
  float l=64;
  Triangle(PVector loc)
  {
    x1=random(loc.x-50, loc.x+50);
    x2=random(loc.x-50, loc.x+50);
    x3=random(loc.x-50, loc.x+50);
    y1=random(loc.y-50, loc.y+50);
    y2=random(loc.y-50, loc.y+50);
    y3=random(loc.y-50, loc.y+50);

    int j = (int)random(0, 5);
    if (j==0) { 
      cr = 5;
      cg = 205;
      cb = 229;
    }
    if (j==1) {
      cr = 255;
      cg = 184;
      cb = 3;
    }
    if (j==2) {
      cr = 255;
      cg = 3;
      cb = 91;
    }
    if (j==3) {
      cr = 61;
      cg = 62;
      cb = 62;
    }
    if (j==4) {
      cr = 214;
      cg = 15;
      cb = 25;
    }
  }
  void update() {
    fill(cr, cg, cb, 4*l);
    l-=2;
    noStroke();
    //triangle(x1, y1, x2, y2, x3, y3);
    ellipse(x1, y1, 128-l*2, 128-l*2);
  }
}